phpcs: ## Run PHP CS (codesniffer)
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpcs --standard=PSR1,$(CI_CONF_DIR)/phpcs.xml --extensions=php --ignore=autoload.php app config resources

phpcpd: ## Run PHP CPD (copy paste detector)
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpcpd app

phpmd: ## Run PHP MD (mess detector)
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpmd app,config,resources text cleancode

phploc: ## Run PHP LOC (lines of code)
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phploc --count-tests app config resources tests

phpunit: ## Run PHPUnit, all tests
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpunit --configuration $(REMOTE_APP_PATH)/phpunit.xml $(REMOTE_APP_PATH)/tests

phpunit_unit: ## Run PHPUnit, unit tests
	$(BIN_DOCKER_COMPOSE) -f $(COMPOSE_FILE_CI) run -v $(APP_PATH):/app $(CONTAINER_CI) phpunit --configuration $(REMOTE_APP_PATH)/phpunit.xml $(REMOTE APP_PATH)/tests/unit

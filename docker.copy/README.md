# Docker lemp stack

## What is this ?

This is a development environment for PHP built in seperate docker containers, managed with docker-compose. The basic setup contains a LEMP stack (Linux, Nginx, MySQL and PHP) 

(MySQL is replaced by a MariaDB instance which is technically the same as MySQL, but with extra features and promises too be faster than MySQL)

## What's inside:
 * Nginx
 * PHP 7.2
    * XDebug
    * Mailcatcher
    * Composer
    * PHPUnit
    * ...
 * MariaDB

## Documentation

wwwdata is the base image (pc_base_image).  If you don't use docker-compose to build your containers,
make sure to build wwwdata first with an image name of pc_base_image.

### Requirements

Make sure you have installed `docker` and `docker-compose`. Both are easily installed via:


Docker: https://docs.docker.com/engine/installation/
   
Docker compose: https://docs.docker.com/compose/install/

### Makefile

In the root of the project there is a `Makefile`, if you are using an OS that support it, 
a lot of commands explained below can be ran easily trough the makefile as a shortcut. If you 
don't support the Makefile you can find each command inside this file. (Variables are defined at the top).

Useful commands ($ make `command`):
    
    # Pull all the newest images
    pull
    # Start development environment. Will create linked containers of services
    up
    # Reload PHP FPM
    reload_php
    # Reload the nginx configuration files (service nginx reload)
    reload_nginx
	
	# Connect to different containers. (Create a bash shell session inside a container)
	connect_mailcatcher
	connect_mariadb
	connect_nginx
	connect_php
	connect_wwwdata
	
	# Build commands (composer, npm, webpack, etc)
	publish: composer (prod), npm, webpack (prod), tar
	publish_dev: composer (dev), npm, webpack (dev)
	composer_up
	composer_up_dev
	composer_install
	npm_install
	webpack
	webpack_dev
	webpack_watch
	
	# Helpers/utilities
	artisan
	phpcs
	phpcpd
	phpmd
	phploc
	tar
	tar_all
	zip

Commands for building and developing the services:

    # Stop and remove all containers shown by 'docker ps -a'
    clear_containers
    # Remove all stored images shown by 'docker images'
    clear_images
    # Will run both 'clear_containers' and 'clear_images"
    clear_all
    # Use the compose build file to build all containers, even unused containers
    build

### Launching containers

    docker-compose up

When supporting the Makefile you can run:

    make up

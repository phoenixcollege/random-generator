<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/13/16
 * Time: 10:34 AM
 */

namespace App\Services\Generators;

use App\Contracts\Generator;

class Random implements Generator
{

    protected $equalize = 26;

    public function generate(array $types, $length = 8, $count = 1)
    {
        $types = array_keys($types);
        $this->validateTypes($types);
        $possible = $this->createPossibleCharacters($types);
        return $this->randomize($possible, $length, $count);
    }

    protected function validateTypes(array $types)
    {
        $allowed = $this->getTypes();
        if (!$types) {
            throw new GeneratorException('A type is required.');
        }
        foreach ($types as $t) {
            if (!array_key_exists($t, $allowed)) {
                throw new GeneratorException("$t is not an allowed type.");
            }
        }
        return true;
    }

    protected function getTypes()
    {
        return [
            'alpha_uc' => [
                'alpha',
                ['uc'],
            ],
            'alpha_lc' => [
                'alpha',
                ['lc'],
            ],
            'int'      => [
                'int',
                [],
            ],
            'spec'     => [
                'spec',
                [],
            ],
        ];
    }

    protected function createPossibleCharacters(array $types)
    {
        $allowed = $this->getTypes();
        $possible = [];
        foreach ($types as $t) {
            $func_data = $allowed[$t];
            $possible = array_merge($possible, call_user_func_array([$this, $func_data[0]], $func_data[1]));
        }
        return $possible;
    }

    protected function randomize($possible, $length, $count)
    {
        $output = [];
        $max_poss = count($possible) - 1;
        for ($i = 0; $i < $count; $i++) {
            $output[] = implode(
                '',
                array_map(
                    function ($v) use ($max_poss, $possible) {
                        $k = random_int(0, $max_poss);
                        return $possible[$k];
                    },
                    range(1, $length)
                )
            );
        }
        return $output;
    }

    public function defaultTypes()
    {
        $types = [];
        foreach ($this->getTypes() as $k => $data) {
            $types[$k] = 1;
        }
        return $types;
    }

    protected function alpha($which)
    {
        $start = $which === 'uc' ? 'A' : 'a';
        $end = $which === 'uc' ? 'Z' : 'z';
        return $this->equalize(range($start, $end));
    }

    protected function equalize($data)
    {
        $max = $this->equalize;
        $c = count($data);
        if ($c && $c < $max) {
            $diff = floor($max / $c);
            for ($i = 1; $i < $diff; $i++) {
                $data = array_merge($data, $data);
            }
        }
        return $data;
    }

    protected function int()
    {
        return $this->equalize(range(0, 9));
    }

    protected function spec()
    {
        return $this->equalize(str_split($this->getSpecialChars()));
    }

    public function getSpecialChars()
    {
        return '!@#$%^&*()';
    }
}

<?php

namespace App\Http\Controllers;

use App\Contracts\Generator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use ValidatesRequests;

    /**
     * @var \App\Contracts\Generator
     */
    protected $generator;

    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @param  Generator  $generator
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $defaults = $this->getDefaults($this->generator);
        $count = $defaults['count'];
        $length = $defaults['length'];
        $types = $defaults['type'];
        return $this->doView($this->generator, $count, $length, $types);
    }

    public function postIndex(Request $request)
    {
        $this->validate($request, $this->getRules(), $this->getDefaults($this->generator));
        $count = (int) $request->get('count', 1);
        $length = (int) $request->get('length', 8);
        $types = $request->get('type', $this->generator->defaultTypes());
        return $this->doView($this->generator, $count, $length, $types);
    }

    protected function doView(Generator $generator, $count, $length, $types)
    {
        $randoms = $generator->generate($types, $length, $count);
        return view('index')
            ->with('randoms', $randoms)
            ->with('count', $count)
            ->with('length', $length)
            ->with('types', $types)
            ->with('generator', $generator);
    }

    protected function getDefaults($generator)
    {
        return ['count' => 1, 'length' => 16, 'type' => $generator->defaultTypes()];
    }

    protected function getRules()
    {
        return [
            'count'  => 'required|int|min:1|max:16',
            'length' => 'required|int|min:1|max:256',
            'type'   => 'required',
        ];
    }

    /**
     * @param  Request  $request
     * @param  array  $rules
     * @param  array  $defaults
     */
    protected function validate(Request $request, array $rules, array $defaults)
    {
        if ($request->get('count', false) !== false && $request->get('length', false) !== false) {
            $validated = $request->validate($rules);
        }
    }
}

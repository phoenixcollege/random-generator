<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/13/16
 * Time: 10:40 AM
 */

namespace App\Contracts;

interface Generator
{

    /**
     * @param array $types
     * @param int $length
     * @param int $count
     * @return string
     */
    public function generate(array $types, $length = 16, $count = 1);

    /**
     * @return array
     */
    public function defaultTypes();

    /**
     * @return string
     */
    public function getSpecialChars();
}

<?php
namespace Tests\Unit\Generator;

class RandomTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \App\Services\Generators\Random
     */
    protected $sut;

    public function setUp(): void
    {
        $this->sut = new \App\Services\Generators\Random();
    }

    public function testBaseGenerate()
    {
        $s = $this->sut->generate($this->sut->defaultTypes());
        $this->assertCount(1, $s);
        $this->assertEquals(8, strlen($s[0]));
    }

    public function testThrowsExceptionForNoType()
    {
        $this->expectException(\App\Services\Generators\GeneratorException::class);
        $s = $this->sut->generate([]);
    }

    public function testThrowsExceptionForInvalidType()
    {
        $this->expectException(\App\Services\Generators\GeneratorException::class);
        $s = $this->sut->generate(['foo' => 1]);
    }

    public function testContainsOnlyUppercase()
    {
        $s = $this->sut->generate(['alpha_uc' => 1]);
        $this->assertRegExp('/[A-Z]{8}/', $s[0]);
    }

    public function testContainsOnlyLowercase()
    {
        $s = $this->sut->generate(['alpha_lc' => 1]);
        $this->assertRegExp('/[a-z]{8}/', $s[0]);
    }

    public function testContainsOnlyNumbers()
    {
        $s = $this->sut->generate(['int' => 1]);
        $this->assertRegExp('/\d{8}/', $s[0]);
    }

    public function testContainsOnlySpecialChars()
    {
        $possible = str_split($this->sut->getSpecialChars());
        $s = $this->sut->generate(['spec' => 1]);
        foreach(str_split($s[0]) as $c) {
            $this->assertContains($c, $possible);
        }
    }

    public function testCountChangesCount()
    {
        $s = $this->sut->generate($this->sut->defaultTypes(), 8, 2);
        $this->assertCount(2, $s);
    }

    public function testLengthChangesLength()
    {
        $s = $this->sut->generate($this->sut->defaultTypes(), 60);
        $this->assertEquals(60, strlen($s[0]));
    }

    public function testLengthCanExceedPossiblesLength()
    {
        $s = $this->sut->generate(['int' => 1], 100);
        $this->assertEquals(100, strlen($s[0]));
    }
}

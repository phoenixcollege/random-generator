<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Random Generator</title>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
</head>
<body>
<section class="section">
    <div class="container">
        <h1>Randomizer</h1>
        @if ($errors->any())
            <div class="message is-danger">
                <div class="message-body">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <form method="post">
            <div class="field">
                <label class="label" for="length">Length</label>
                <input type="number" class="input" name="length" value="{{ $length }}" min="1" max="256" maxlength="3">
                <small class="help">Max 256</small>
            </div>
            <div class="field">
                <label class="label" for="count">Count</label>
                <input type="number" class="input" name="count" value="{{ $count }}" min="1" max="16" maxlength="2">
                <small class="help">Max 16</small>
            </div>
            <div class="field">
                <div class="control">
                    <label class="checkbox" for="type-alpha-uc-1">
                        <input type="checkbox" value="1" id="type-alpha-uc-1"
                               name="type[alpha_uc]" {{ isset($types['alpha_uc']) ? 'checked' : null }}>
                        UPPERCASE
                    </label>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <label class="checkbox" for="type-alpha-lc-1">
                        <input type="checkbox" value="1" id="type-alpha-lc-1"
                               name="type[alpha_lc]" {{ isset($types['alpha_lc']) ? 'checked' : null }}>
                        lowercase
                    </label>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <label class="checkbox" for="type-int-1">
                        <input type="checkbox" value="1" id="type-int-1"
                               name="type[int]" {{ isset($types['int']) ? 'checked' : null }}>
                        Numbers (0-9)
                    </label>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <label class="checkbox" for="type-spec-1">
                        <input type="checkbox" value="1" id="type-spec-1"
                               name="type[spec]" {{ isset($types['spec']) ? 'checked' : null }}>
                        Special Characters ({{ $generator->getSpecialChars() }})
                    </label>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <button type="submit" class="button">Generate</button>
                </div>
            </div>
        </form>
        <hr/>
        <fieldset>
            <ul>
                @foreach($randoms as $random)
                    <li>{{ $random }}</li>
                @endforeach
            </ul>
        </fieldset>
    </div>
</section>
</body>
</html>
